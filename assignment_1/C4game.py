import gym
import random
import requests
import numpy as np
from gym_connect_four import ConnectFourEnv
import math as m

env: ConnectFourEnv = gym.make("ConnectFour-v0")

#SERVER_ADRESS = "http://localhost:8000/"
SERVER_ADRESS = "https://vilde.cs.lth.se/edap01-4inarow/"
API_KEY = 'nyckel'
STIL_ID = ["to5185so-s"] # TODO: fill this list with your stil-id's

def call_server(move):
   res = requests.post(SERVER_ADRESS + "move",
                       data={
                           "stil_id": STIL_ID,
                           "move": move, # -1 signals the system to start a new game. any running game is counted as a loss
                           "api_key": API_KEY,
                       })
   # For safety some respose checking is done here
   if res.status_code != 200:
      print("Server gave a bad response, error code={}".format(res.status_code))
      exit()
   if not res.json()['status']:
      print("Server returned a bad status. Return message: ")
      print(res.json()['msg'])
      exit()
   return res

"""
You can make your code work against this simple random agent
before playing against the server.
It returns a move 0-6 or -1 if it could not make a move.
To check your code for better performance, change this code to
use your own algorithm for selecting actions too
"""
def opponents_move(env):
   env.change_player() # change to oppoent
   avmoves = env.available_moves()
   if not avmoves:
      env.change_player() # change back to student before returning
      return -1

   # TODO: Optional? change this to select actions with your policy too
   # that way you get way more interesting games, and you can see if starting
   # is enough to guarrantee a win
   action = random.choice(list(avmoves))

   state, reward, done, _ = env.step(action)
   if done:
      if reward == 1: # reward is always in current players view
         reward = -1
   env.change_player() # change back to student before returning
   return state, reward, done

# ---------------------------------------------------------------------------------------------------------------------------------
BOARD_ROWS = 6
BOARD_COLS = 7

def student_move(env, state):
   """
   TODO: Implement your min-max alpha-beta pruning algorithm here.
   Give it whatever input arguments you think are necessary
   (and change where it is called).
   The function should return a move from 0-6
   """
   print("before choice")
   move, value = minimax(env, state, 4, -m.inf, m.inf, True)
   print("move: ", move)

   return move

#Returns the value of the best choice.
def minimax(env, state, depth, alpha, beta, maximizingPlayer): #my fictional environment, depth in search tree, alpha, beta, min or max player
   avmoves = env.available_moves() #availble moves
   terminalnode = env.is_win_state() or avmoves == 0 #check if node is terminal

   if depth == 0 or terminalnode:
      env.change_player()
      return 1, evaluate(state) # return evaluation value of the current state

   chosen_move = 3

   if maximizingPlayer:
      max_value = -m.inf
      
      for move in avmoves: #try outcome for all possible moves
         orig_state = state.copy() #save the board as it is rn
         board, result, done, _ = env.step(move) #make the move
         
         env.change_player() #the other players turn
         n, value = minimax(env, board, depth - 1, alpha, beta, False) #"value" is the value  will be thethis is the latest evaluation
         env.reset(board=orig_state) #remove the move

         if value > max_value:
            max_value = value
            chosen_move = move
         alpha = max(alpha, value)
         if alpha >= beta:
            break   
      return chosen_move, max_value

   else:
      min_value = m.inf

      for move in avmoves:
         orig_state = state.copy() #save the board as it is rn
         board, result, done, _ = env.step(move) #make the move

         env.change_player() #the other persons turn
         n, value = minimax(env, board, depth - 1, alpha, beta, True)
         env.reset(board=orig_state) #remove the move
         env.change_player() #reset sets the player to 1, so we need to change back to -1 again.

         if value < min_value:
            min_value = value
            chosen_move = move
         beta = min(beta, value)
         if beta <= alpha:
            break
      return chosen_move, min_value


#Evaluation function. Evaluates a certain board. Returns points (int) of the board.
def evaluate(board):
   points = 0
   
   #score for center column
   center_count = 0
   zeros_count = 0
   for i in range(BOARD_ROWS):
      if board[i][3] == 1:
         center_count +=1
   if board[0][3] == -1 or board[1][3] == -1 or board[2][3] == -1 or board[3][3]:
      center_count = 0
   points += center_count * 3 

   # ROWS. Create arrays with length 4 of the rows and add the calculated score for each to "points"
   for i in range(BOARD_ROWS):
      for j in range(BOARD_COLS - 3):
         four_array = [board[i][j], board[i][j+1], board[i][j+2], board[i][j+3]]
         points += calc_score(four_array)
   
   # COLS. Create arrays with length 4 of the cols and add the calculated score for each to "points"
   #reversed_board = [list(i) for i in zip(board)]
   for i in range(BOARD_COLS):
      for j in range(BOARD_ROWS - 3):
         four_array2 = [board[j][i], board[j+1][i], board[j+2][i], board[j+3][i]]
         points += calc_score(four_array2)

   # DIAGONALS. Create arrays with length 4 of the rows and add the calculated score for each to "points"
   reversed_board = np.fliplr(board)
   for i in range(BOARD_ROWS - 3):
      for j in range(BOARD_COLS - 3):
            four_array3 = [board[i][j], board[i + 1][j + 1], board[i + 2][j + 2], board[i + 3][j + 3]]
            four_array4 = [reversed_board[i][j], reversed_board[i + 1][j + 1], reversed_board[i + 2][j + 2], reversed_board[i + 3][j + 3]]
            points += calc_score(four_array3)
            points += calc_score(four_array4)

   return points        

def calc_score(array):
   points = 0
   array_sum = np.sum(array) #sum the values in the array
   if array_sum == 4: #4 ones
      points += 100
   elif array_sum == 3: #3 ones and 1 zero
      points += 20
   elif array_sum == 2: #2 ones and 2 zeros
      nbr_i = 0
      for i in range(3):
         if array[i] == 0:
            nbr_i += 1
      if nbr_i == 2:
         points += 2
   elif array_sum == -3: #3 minus-ones and one zero
      points -= 150
   
   return points



# -------------------------------------------------------------------------------------------------------------------------------------
def play_game(vs_server = False):
   """
   The reward for a game is as follows. You get a
   botaction = random.choice(list(avmoves)) reward from the
   server after each move, but it is 0 while the game is running
   loss = -1
   win = +1
   draw = +0.5
   error = -10 (you get this if you try to play in a full column)
   Currently the player always makes the first move
   """

   # default state
   state = np.zeros((6, 7), dtype=int)

   # setup new game
   if vs_server:
      # Start a new game
      res = call_server(-1) # -1 signals the system to start a new game. any running game is counted as a loss

      # This should tell you if you or the bot starts
      print(res.json()['msg'])
      botmove = res.json()['botmove']
      state = np.array(res.json()['state'])
   else:
      # reset game to starting state
      env.reset(board=None)
      # determine first player
      student_gets_move = random.choice([True, False])
      if student_gets_move:
         print('You start!')
         print()
      else:
         print('Bot starts!')
         print()

   # Print current gamestate
   print("Current state (1 are student discs, -1 are servers, 0 is empty): ")
   print(state)
   print()

   done = False
   while not done:
      # Select your move
      stmove = student_move(env, state) # TODO: change input here

      # make both student and bot/server moves
      if vs_server:
         # Send your move to server and get response
         res = call_server(stmove)
         print(res.json()['msg'])

         # Extract response values
         result = res.json()['result']
         botmove = res.json()['botmove']
         state = np.array(res.json()['state'])
         # set servers move in env
         env.reset(state)

      else:
         if student_gets_move:
            # Execute your move
            avmoves = env.available_moves()
            if stmove not in avmoves:
               print("You tied to make an illegal move! Games ends.")
               break
            state, result, done, _ = env.step(stmove)

         student_gets_move = True # student only skips move first turn if bot starts

         # print or render state here if you like

         # select and make a move for the opponent, returned reward from students view
         if not done:
            state, result, done = opponents_move(env)

      # Check if the game is over
      if result != 0:
         done = True
         if not vs_server:
            print("Game over. ", end="")
         if result == 1:
            print("You won!")
         elif result == 0.5:
            print("It's a draw!")
         elif result == -1:
            print("You lost!")
         elif result == -10:
            print("You made an illegal move and have lost!")
         else:
            print("Unexpected result result={}".format(result))
         if not vs_server:
            print("Final state (1 are student discs, -1 are servers, 0 is empty): ")
         return result
      else:
         print("Current state (1 are student discs, -1 are servers, 0 is empty): ")

      # Print current gamestate
      print(state)
      print()

def main():
   winstreak = 0
   lose = 0
   #for i in range(20):
   result = play_game(vs_server = True)
   if (result == 1 or result == 0.5):
      winstreak += 1
   elif result == -1:
      lose += 1
   print("current win/lose: ", winstreak, lose)

   # TODO: Change vs_server to True when you are ready to play against the server
   # the results of your games there will be logged

if __name__ == "__main__":
    main()
